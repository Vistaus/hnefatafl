/*
 * Copyright (C) 2021 - 2022 Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hnefatafl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root
    signal startNewGame(var attackerSetting, var defenderSetting)

    ColumnLayout {
        spacing: 2
        anchors.fill: parent

        ComboBox {
            id: playersSelectionCb
            font.pixelSize: height * 0.35
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            Layout.margins: width / 10
            model: [
                i18n.tr("Human player as attackers vs. AI"),
                i18n.tr("Human player as defenders vs. AI"),
                i18n.tr("Two human players")
            ]
            
    	    popup: Popup {
	    	    width: playersSelectionCb.width
    		    implicitHeight: contentHeight
		        padding: 0
		        margins: 0
		        spacing: 0
		        font.pixelSize: playersSelectionCb.height * 0.35
    	    	contentItem: ListView {
    	    	    clip: true
    	    	    spacing: 0
    	    	    implicitHeight: contentHeight
    	    	    model: playersSelectionCb.popup.visible ? playersSelectionCb.delegateModel : null
	        	    currentIndex: playersSelectionCb.highlightedIndex
        		}
        	}
        }

        Button {
            Layout.alignment: Qt.AlignCenter
            Layout.margins: width / 10
            text: i18n.tr('New Game')
            onClicked: {
                if (playersSelectionCb.currentIndex === 0) {
                    root.startNewGame("Human", "AI")
                    stack.push(gamePage)
                }
                else if (playersSelectionCb.currentIndex === 1) {
                    root.startNewGame("AI", "Human")
                    stack.push(gamePage)
                }
                else if (playersSelectionCb.currentIndex === 2) {
                    root.startNewGame("Human", "Human")
                    stack.push(gamePage)
                }
            }
        }
    }
}
