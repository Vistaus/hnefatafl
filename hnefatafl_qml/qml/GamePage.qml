/*
 * Copyright (C) 2021 - 2022 Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hnefatafl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import QtQuick.Dialogs 1.3
import QmlTaflEngineInterface 1.0

Page
{
    id: root
    signal boardChanged()
    signal unselectPiece()
    signal startNewGame(var attackerSetting, var defenderSetting)

    property string attackersPlayerSetting : ""
    property string defendersPlayerSetting : ""

    MessageDialog {
        id: endGameDialog
        title: "Game ended!"
        text: "Somebody just won!"
        onAccepted: {
            stack.pop()
        }
    }

    Popup {
        id: endGamePopup
        modal: false

        x: root.height > root.width ? root.width * 0.25 : gameBoard.x + gameBoard.width
        y: root.height > root.width ? gameBoard.y + gameBoard.height : root.height * 0.25
        width: root.width < root.height ? root.width / 2 : root.height / 2
        height: root.width < root.height ? root.width / 2 : root.height / 2
        dim: true

        onClosed: {
            stack.pop()
        }
        ColumnLayout {
            anchors.fill: parent
            Text {
                id: endGamePopupText
                text: ""
                color: theme.palette.normal.foregroundText
                wrapMode: Text.WordWrap
                Layout.leftMargin: parent.height / 10
                Layout.rightMargin: parent.height / 10
                Layout.topMargin: parent.height / 10
                Layout.fillWidth: true
            }
            Button {
                text: "OK"
                Layout.margins: parent.height / 10
                Layout.fillWidth: true
                onClicked: {
                    endGamePopup.close()
                    stack.pop()
                }
            }
        }
    }

    QmlTaflEngineInterface {
        id: taflEngine
    }

    Grid
    {
        columns: root.availableWidth < root.availableHeight ? 1 : 2
        rows: root.availableWidth < root.availableHeight ? 2 : 1
        padding:  parent.width < parent.height ? parent.width * 0.05 : parent.height * 0.05
        spacing: parent.width < parent.height ? parent.width * 0.05 : parent.height * 0.05
        anchors.fill: parent
        horizontalItemAlignment: Grid.AlignHCenter
        verticalItemAlignment: Grid.AlignVCenter

        GridLayout {
            id: gameBoard
            property int boardRows : 7
            property int boardColumns : 7
            property string selectedTile : ""

            width: parent.width < parent.height ? parent.width * 0.9 : parent.height * 0.9
            height: parent.width < parent.height ? parent.width * 0.9 : parent.height * 0.9

            columns: 7
            Repeater {
                model: 49
                Rectangle {
                    property int tileX: index % gameBoard.boardColumns
                    property int tileY: (gameBoard.boardRows - 1) - Math.trunc(index / gameBoard.boardRows)
                    property int myIndex: index
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "gray"
                    Text {
                        id: pieceCharacter
                        text: ""
                        font.pixelSize: parent.width
                        color: "black"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (taflEngine.is_tile_occupied_by_current_player(parent.tileX, parent.tileY)) {
                                root.unselectPiece();
                                pieceCharacter.color = "orange";
                                gameBoard.selectedTile = tileNumCoordsToStringCoords(parent.tileX, parent.tileY);
                            } else if (gameBoard.selectedTile != "") {
                                var move = gameBoard.selectedTile + tileNumCoordsToStringCoords(parent.tileX, parent.tileY);
                                if (taflEngine.make_move(move)) {
                                    gameBoard.selectedTile = "";
                                    root.unselectPiece();
                                    root.boardChanged();
                                }
                            }
                        }
                    }

                    function tileNumCoordsToStringCoords(tileX, tileY) {
                        return "ABCDEFGHIJKLM"[tileX] + (tileY + 1).toString();
                    }

                    function updateTile() {
                        if (taflEngine.is_corner(tileX, tileY)) {
                            color = "red";
                        } else if (taflEngine.is_throne(tileX, tileY)) {
                            color = "yellow";
                        } else {
                            color = "gray";
                        }

                        var pieceStr = taflEngine.get_tile_content(tileX, tileY);
                        //console.log(pieceStr);
                        if (pieceStr.localeCompare("King") === 0) {
                            pieceCharacter.text = "\u{2654}";
                        } else if (pieceStr.localeCompare("Defender") === 0) {
                            pieceCharacter.text = "\u{2659}";
                        } else if (pieceStr.localeCompare("Attacker") === 0) {
                            pieceCharacter.text = "\u{265F}";
                        } else if (pieceStr.localeCompare("None") === 0) {
                            pieceCharacter.text = "";
                        }
                    }

                    function unselect() {
                        pieceCharacter.color = "black";
                        gameBoard.selectedTile = "";
                    }

                    Component.onCompleted: {
                        root.boardChanged.connect(updateTile)
                        root.unselectPiece.connect(unselect)
                    }
                }
            }
            function afterUpdateChecks() {
                var winner = taflEngine.check_winner();
                if (winner === "Attackers") {
                    //endGameDialog.text = "Attackers won!";
                    //endGameDialog.visible = true;
                    endGamePopupText.text = "Attackers won!"
                    endGamePopup.open()
                } else if (winner === "Defenders") {
                    //endGameDialog.text = "Defenders won!";
                    //endGameDialog.visible = true;
                    endGamePopupText.text = "Defenders won!"
                    endGamePopup.open()
                }

                if ((taflEngine.current_player() === "Attackers" && root.attackersPlayerSetting === "AI") ||
                    (taflEngine.current_player() === "Defenders" && root.defendersPlayerSetting === "AI")) {
                    taflEngine.make_ai_move(4);
                    root.boardChanged();
                }
            }

            function startNewGameHandler(attackerSetting, defenderSetting ) {
                root.attackersPlayerSetting = attackerSetting;
                root.defendersPlayerSetting = defenderSetting;
                taflEngine.set_up_board();
                root.boardChanged();
            }

            Component.onCompleted: {
                root.boardChanged.connect(afterUpdateChecks)
            }
        }

        Button {
            text: i18n.tr('Exit game')
            onClicked: {
                stack.pop()
            }
        }
    }
    Component.onCompleted: {
        root.startNewGame.connect(gameBoard.startNewGameHandler)
    }
}
