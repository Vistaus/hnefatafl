/*
 * Copyright (C) 2020 - 2022 Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import Ubuntu.Components 1.3 as UITK

ListView
{
    id: root
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    width: parent.width - getMarginSpaceForItemSize(parent.width, parent.height)
    height: parent.height - getMarginSpaceForItemSize(parent.width, parent.height)
    spacing: 2


    Component
    {
        id: basicAppInfo
        Column
        {
            width: root.width
            UITK.UbuntuShape {
                width: getMaximum(root.width, root.height) / 5
                height: width
                radius: "large"
                anchors.horizontalCenter: parent.horizontalCenter
                source: Image {
                    mipmap: true
                    source: "qrc:/assets/logo.svg"
                }
            }

            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr('Hnefatafl')
                height: getMaximum(root.width, root.height) / 15

                font.pixelSize: height * 0.8
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Version") + " 0.1.1" //+ Qt.application.version
                height: getMaximum(root.width, root.height) / 30
                font.pixelSize: height * 0.8
            }
        }
    }

    Component
    {
        id: linkReference
        Rectangle
        {
            width: parent.width
            height: getMaximum(root.width, root.height) / 20
            color: theme.palette.normal.foreground
            radius: height / 3
            border.color: theme.palette.normal.foregroundText
            border.width: 1
            Label
            {
                text: splitCsvString(modelContent)[0]
                anchors.left: parent.left
                anchors.right: nextSymbol.left
                height: parent.height
                padding: parent.height / 5
                fontSizeMode: Text.Fit
                font.pixelSize: height
                MouseArea
                {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally(splitCsvString(modelContent)[1])
                }
            }
            UITK.Icon
            {
                id: nextSymbol
                name: "next"
                width: parent.height * 0.8
                height: parent.height * 0.8
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                color: theme.palette.normal.foregroundText
                MouseArea
                {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally(splitCsvString(modelContent)[1])
                }
            }

            function splitCsvString(str)
            {
                return str.split(",")
            }
        }
    }

    Component
    {
        id: appInfoItem
        Rectangle
        {
            width: parent.width
            height: getMaximum(root.width, root.height) / 20
            color: "lightgrey"
            radius: height / 3
            border.color: "black"
            border.width: 1
            Text
            {
                text: modelContent
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height
                padding: parent.height / 5
                fontSizeMode: Text.Fit
                font.pixelSize: height
                MouseArea
                {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally(splitCsvString(modelContent)[1])
                }
            }
        }
    }


    delegate: Component
    {
        Loader
        {
            width: root.width
            property string modelContent: modelData.content
            property int modelItemIndex: modelData.index
            sourceComponent:
            {
                switch(modelData.type)
                {
                    case "basicAppInfo":
                        return basicAppInfo
                    case "appInfoItem":
                        return appInfoItem
                    case "linkReference":
                        return linkReference
                }
            }
        }
    }

    function getMarginSpaceForItemSize(width, height)
    {
        return Math.min(width, height) /10
    }

    function getMinimum(first, second)
    {
        return Math.min(first, second)
    }

    function getMaximum(first,second)
    {
        return Math.max(first, second)
    }

}
