/*
 * Copyright (C) 2022  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hnefatafl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page
{
    CustomComponentsListView
    {
        spacing: 10
        model:
        [
            {content: "", type: "basicAppInfo", index: 0},
            {content: i18n.tr("Website with game rules") + ",https://www.ancientgames.org/hnefatafl-brandubh/", type: "linkReference" , index: 0},
            {content: i18n.tr("Source code") + ",https://gitlab.com/andrisk/hnefatafl", type: "linkReference" , index: 0},
            {content: i18n.tr("Engine source code") + ",https://gitlab.com/andrisk/tafl-engine", type: "linkReference" , index: 0},
            {content: i18n.tr("Report issue")  + ",https://gitlab.com/andrisk/hnefatafl/issues", type: "linkReference" , index: 0},
            {content: i18n.tr("License") + ": GPLv3,https://www.gnu.org/licenses/gpl-3.0.html", type: "linkReference" , index: 0},
            {content: i18n.tr("Author") + ": Andrej Trnkóci,https://gitlab.com/andrisk/", type: "linkReference", index: 0},
            {content: i18n.tr("Translation") + " (fr): Anne Onyme,https://gitlab.com/Anne17", type: "linkReference", index: 0},
        ]
    }
}
